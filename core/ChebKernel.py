import networkx as nx
import numpy as np
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim.lr_scheduler import StepLR

from scipy.sparse import csr_matrix, csc_matrix, coo_matrix, lil_matrix
#import scipy.sparse.linalg.eigs as eigs
from scipy.sparse.linalg import eigs
from core.graph_util import generate_kernel, generate_sparse_kernel
from core.SparseTensor import is_sparse, SparseTensor

class ChebKernel(nn.Module):

    def __init__(self, S):
        super().__init__()

        self.S = S
        self.w = nn.Parameter(torch.rand((S + 1), dtype=torch.float))

    @staticmethod
    def base_Ls(A: SparseTensor, Lhop:int) -> torch.Tensor:

        N = A.shape[0]
        sI = generate_sparse_kernel(A, kernel_name='I')
        I = torch.eye(N, dtype=torch.float)
        sL = generate_sparse_kernel(A, kernel_name='DLD')

        # convert to scipy.sparse
        data = sL._values()
        row, column = sL._indices()
        sp_coo = coo_matrix((data, (row, column)), shape=(N,N))
        sp_csr = csr_matrix(sp_coo)

        # obtain eigen_value
        w = eigs(sp_csr, k=1, return_eigenvectors=False)[0].real

        assert w < 2, 'wrong'

        # normalized
        stL = 2 * sL / w - sI

        # generate lists
        dDs = [I, stL.to_dense()]


        for i in range(2, Lhop + 1):
            dLs_1 = dDs[-1]
            dLs_2 = dDs[-2]
            dLi= 2 * torch.sparse.mm(stL, dLs_1) - dLs_2
            dDs.append(dLi)

        return torch.stack(dDs, dim=2) # [N,N,Lhop]

    @staticmethod
    def mask_from_A(A:torch.Tensor, S:int) -> torch.Tensor:
        # N,N,D -> N,N
        N = A.shape[0]
        I = torch.eye(N, dtype=torch.float)
        sA: SparseTensor = A.to_sparse()

        # computes reachable nodes
        Al = I + A
        Ai_1 = A
        for i in range(2, S + 1):
            Ai = torch.sparse.mm(sA, Ai_1)
            Al += Ai
            Ai_1 = Ai

        # gen mask
        mask = torch.ones((N, N), dtype=torch.float)
        mask[Al == 0] = 0
        return mask

    def forward(self, As: [torch.Tensor], Ds: [SparseTensor] = None) -> [torch.Tensor]:
        raise NotImplementedError

    def forward_sparse(self, idx:torch.Tensor, valD: torch.Tensor) -> SparseTensor:
        """
        :param N: size of the graph
        :param valD: values on indices [W, S + 1]
        :param idx: indices where Mask is non-zero, [2, W]
        :return:
        """
        valy = self.w.unsqueeze(0) * valD # W, S+1
        valy = torch.sum(valy, dim=1) # W

        sK = torch.sparse.FloatTensor(idx, valy)  # [N,N]
        return sK

