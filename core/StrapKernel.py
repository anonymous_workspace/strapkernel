import networkx as nx
import numpy as np
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim.lr_scheduler import StepLR

from core.SparseTensor import is_sparse, SparseTensor


def fib(n):
    if n < 3: return 1
    a = 1
    b = 1
    for i in range(3, n + 1):
        x = a + b
        a = b
        b = x
    return b


def Q(l):
    return fib(l + 4) - 2


class Mish(nn.Module):
    def forward(self, x):
        return x * torch.tanh(F.softplus(x))


class StrapKernel(nn.Module):

    def __init__(self, L, S, phi_p: int = 2, phi_d: int = 16, allow_loop=True, plus_only=False):
        super().__init__()

        assert S <= L, "S:%d requires not to be larger than L:%d" % (S, L)
        self.L = L
        self.S = S
        self.allow_loop = allow_loop

        top = []
        d_top = Q(L)
        for i in range(phi_p - 1):
            top.append(nn.BatchNorm1d(d_top))
            top.append(nn.Linear(d_top, phi_d))
            top.append(Mish())
            d_top = phi_d
        top.append(nn.BatchNorm1d(d_top))
        top.append(nn.Linear(d_top, 1))

        if plus_only:
            top.append(nn.Softplus())

        self.phi = nn.Sequential(*top)

        self.parameters_fixed = False


    def fix_parameters(self, flag):
        for param in self.parameters():
            param.requires_grad = not flag
        self.parameters_fixed = flag

    @staticmethod
    def base_monomials(A: torch.Tensor, L: int) -> torch.Tensor:
        # N,N -> N,N,Q
        N = A.shape[0]
        I = torch.eye(N, dtype=torch.float)
        J = torch.ones((N, N,), dtype=torch.float)

        if L == 0:
            return I.unsqueeze(2)  # N,N,1
        elif L == 1:
            return torch.stack([I, J, A], dim=2)

        Z = torch.zeros((N, N,), dtype=torch.float)
        sA: SparseTensor = A.to_sparse()

        # new
        Ls = [I, N * J, A]
        J_heads = [J]
        A_heads = [A]

        for i in range(2, L + 1):
            new_J_heads = []
            new_A_heads = []
            for X in J_heads:
                # AX
                Y = torch.sparse.mm(sA, X)
                Ls.append(Y)
                new_A_heads.append(Y)
            for X in A_heads:
                # JX
                Y = Z + torch.sum(X, dim=0).unsqueeze(0)
                Ls.append(Y)
                new_J_heads.append(Y)
                # AX
                Y = torch.sparse.mm(sA, X)
                Ls.append(Y)
                new_A_heads.append(Y)

            A_heads = new_A_heads
            J_heads = new_J_heads
        # stacking
        D = torch.stack(Ls, dim=2)  # N,N, Q

        return D

    @staticmethod
    def mask_from_A(A: torch.Tensor, S: int, allow_loop: bool) -> torch.Tensor:
        # N,N,D -> N,N
        N = A.shape[0]
        I = torch.eye(N, dtype=torch.float)
        sA: SparseTensor = A.to_sparse()

        # computes reachable nodes
        Al = I + A
        Ai_1 = A
        for i in range(2, S + 1):
            Ai = torch.sparse.mm(sA, Ai_1)
            Al += Ai
            Ai_1 = Ai

        # gen mask
        mask = torch.ones((N, N), dtype=torch.float)
        if not allow_loop:
            mask = mask - I
        mask[Al == 0] = 0
        return mask

    def forward_sparse(self, idx: torch.Tensor, valD: torch.Tensor) -> SparseTensor:
        """
        :param idx: indices where Mask is non-zero, [2, W]
        :param valD: values on indices [W, Q(L)]
        :return:
        """
        valy = self.phi(valD)  # W, 1
        sK = torch.sparse.FloatTensor(idx, valy[:, 0])  # [N,N]
        return sK

