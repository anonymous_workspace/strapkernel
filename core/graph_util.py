import sys, os

#sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
#print(sys.path)

import torch

from core.SparseTensor import is_sparse, SparseTensor


def sparse_dense_mul(s: SparseTensor, d: torch.Tensor) -> SparseTensor:
    i = s._indices()
    v = s._values()
    dv = d[i[0, :], i[1, :]]  # get values from relevant entries of dense matrix
    v = v * dv
    # discard 0
    #nonzero = torch.where(v != 0)[0]
    #i = i[:,nonzero]
    #v = v[nonzero]
    return torch.sparse.FloatTensor(i, v)

# for sparse
def sparse_get_degree(idx: torch.Tensor) -> torch.Tensor:
    row, col = idx
    deg = torch.bincount(row).to(torch.float)
    return deg


def sparse_get_degree_with_weight(idx: torch.Tensor, weight: torch.Tensor) -> torch.Tensor:
    row, col = idx
    N = torch.max(idx[0]) + 1
    W = weight.shape[0]
    Z = torch.zeros((W, N), dtype=torch.float)
    Z[torch.arange(W, dtype=torch.long), row.long()] = 1
    deg = torch.matmul(Z.transpose(0, 1), weight)
    return deg


def sparse_self_loop_idx(N: int):
    i = torch.arange(N, dtype=torch.long).view(1, -1)  # 1, N
    self_loops = torch.cat((i, i), dim=0)  # 2, N
    return self_loops


def sparse_Dx_normalization(idx, deg, weight):
    row, _ = idx
    deg_inv = torch.pow(deg, -1)
    deg_inv[deg_inv == float('inf')] = 0.0
    weight = deg_inv[row] * weight
    return weight


def sparse_DxD_normalization(idx, deg, weight):
    row, col = idx
    deg_inv_sqrt = torch.pow(deg, -0.5)
    deg_inv_sqrt[deg_inv_sqrt == float('inf')] = 0.0
    weight = deg_inv_sqrt[row] * weight * deg_inv_sqrt[col]
    return weight


def generate_sparse_kernel(A: SparseTensor, kernel_name: str) -> SparseTensor:
    if kernel_name == 'A':
        # adjacency matrix kernel
        K = A
    elif kernel_name == 'I':
        N = A.shape[0]
        idx = sparse_self_loop_idx(N)  # 2, N
        weight = torch.ones(N, dtype=torch.float)  # N
        # convert to sparse matrix
        K = torch.sparse.FloatTensor(idx, weight)
    elif kernel_name == 'tA':
        # adjacency with self-loop kernel
        N = A.shape[0]
        idx: torch.Tensor = A._indices()  # 2, W
        weight = torch.ones(idx.shape[1])

        # loop
        self_loops = sparse_self_loop_idx(N)  # 2, N
        weight_loops = torch.ones(N)  # N

        # concat
        idx = torch.cat((idx, self_loops), dim=1)  # 2, W + N
        weight = torch.cat((weight, weight_loops), dim=0)  # W + N

        # convert to sparse matrix
        K = torch.sparse.FloatTensor(idx, weight)
    elif kernel_name == 'tT':
        dA = A.to_dense()
        A2 = torch.sparse.mm(A, dA)
        T = sparse_dense_mul(A, A2)
        idx = T._indices()
        weight = T._values()

        N = A.shape[0]

        # loop
        self_loops = sparse_self_loop_idx(N)  # 2, N
        weight_loops = torch.ones(N)  # N

        # concat
        idx = torch.cat((idx, self_loops), dim=1)  # 2, W + N
        weight = torch.cat((weight, weight_loops), dim=0)  # W + N

        # convert to sparse matrix
        K = torch.sparse.FloatTensor(idx, weight)

    elif kernel_name == 'L':
        # D - A
        N = A.shape[0]
        idx: torch.Tensor = A._indices()  # 2, W
        deg = sparse_get_degree(idx)
        weight = torch.ones(idx.shape[1], dtype=torch.float) * (-1)  # W

        # D
        self_loops = sparse_self_loop_idx(N)  # 2, N
        weight_loops = deg.float()  # N

        # concat
        idx = torch.cat((idx, self_loops), dim=1)  # 2, W + N
        weight = torch.cat((weight, weight_loops), dim=0)  # W + N

        # convert to sparse matrix
        K = torch.sparse.FloatTensor(idx, weight)

    elif kernel_name == 'DA':
        # A
        idx: torch.Tensor = A._indices()  # 2, W
        deg = sparse_get_degree(idx)
        weight = torch.ones(idx.size(1))

        # Dx normalization
        weight = sparse_Dx_normalization(idx, deg, weight)

        K = torch.sparse.FloatTensor(idx, weight)


    elif kernel_name == 'DtA':
        # adjacency with self-loop kernel
        N = A.shape[0]
        idx: torch.Tensor = A._indices()  # 2, W
        weight = torch.ones(idx.size(1))

        # loop
        self_loops = sparse_self_loop_idx(N)  # 2, N
        weight_loops = torch.ones(N)  # N

        # concat
        idx = torch.cat((idx, self_loops), dim=1)  # 2, W + N
        weight = torch.cat((weight, weight_loops), dim=0)  # W + N

        # update degree!!
        deg = sparse_get_degree(idx)

        # Dx normalization
        weight = sparse_Dx_normalization(idx, deg, weight)

        K = torch.sparse.FloatTensor(idx, weight)
    elif kernel_name == 'tTs':
        # sign(I + T)

        dA = A.to_dense()
        A2 = torch.sparse.mm(A, dA)
        T = sparse_dense_mul(A, A2)
        idx = T._indices()
        #weight = torch.ones(idx.shape[1]) # one

        nonzero = torch.where(T._values() != 0)[0]
        idx = idx[:,nonzero]
        weight = torch.ones(idx.shape[1])#[nonzero]

        N = A.shape[0]

        # loop
        self_loops = sparse_self_loop_idx(N)  # 2, N
        weight_loops = torch.ones(N)  # N

        # concat
        idx = torch.cat((idx, self_loops), dim=1)  # 2, W + N
        weight = torch.cat((weight, weight_loops), dim=0)  # W + N

        # convert to sparse matrix
        K = torch.sparse.FloatTensor(idx, weight)
    elif kernel_name == 'DtT':
        # weight on Triangle
        # difficult...
        # use dense....

        dA = A.to_dense()
        A2 = torch.sparse.mm(A, dA)
        T = sparse_dense_mul(A, A2)
        idx = T._indices()
        weight = T._values()

        N = A.shape[0]

        # loop
        self_loops = sparse_self_loop_idx(N)  # 2, N
        weight_loops = torch.ones(N)  # N

        # concat
        idx = torch.cat((idx, self_loops), dim=1)  # 2, W + N
        weight = torch.cat((weight, weight_loops), dim=0)  # W + N

        # update degree!!
        deg = sparse_get_degree_with_weight(idx, weight)

        # DxD normalization
        weight = sparse_Dx_normalization(idx, deg, weight)

        # convert to sparse matrix
        K = torch.sparse.FloatTensor(idx, weight)
    elif kernel_name == 'DL':
        # - A
        N = A.shape[0]
        idx: torch.Tensor = A._indices()  # 2, W
        deg = sparse_get_degree(idx)
        weight = torch.ones(idx.shape[1], dtype=torch.float) * (-1)  # W

        # D
        self_loops = sparse_self_loop_idx(N)  # 2, N
        weight_loops = deg.float()  # N

        # D - A
        idx = torch.cat((idx, self_loops), dim=1)  # 2, W + N
        weight = torch.cat((weight, weight_loops), dim=0)  # W + N

        # Dx normalization
        weight = sparse_Dx_normalization(idx, deg, weight)

        # convert to sparse matrix
        K = torch.sparse.FloatTensor(idx, weight)

    elif kernel_name == 'DAD':
        # A
        # N = A.shape[0]
        idx: torch.Tensor = A._indices()  # 2, W
        deg = sparse_get_degree(idx)
        weight = torch.ones(idx.size(1))

        # DxD normalization
        weight = sparse_DxD_normalization(idx, deg, weight)

        K = torch.sparse.FloatTensor(idx, weight)

    elif kernel_name == 'DtAD':
        # adjacency with self-loop kernel
        N = A.shape[0]
        idx: torch.Tensor = A._indices()  # 2, W
        # deg = get_degree(idx)
        weight = torch.ones(idx.size(1))

        # loop
        self_loops = sparse_self_loop_idx(N)  # 2, N
        weight_loops = torch.ones(N)  # N

        # concat
        idx = torch.cat((idx, self_loops), dim=1)  # 2, W + N
        weight = torch.cat((weight, weight_loops), dim=0)  # W + N

        # update degree!!
        deg = sparse_get_degree(idx)

        # DxD normalization
        weight = sparse_DxD_normalization(idx, deg, weight)

        K = torch.sparse.FloatTensor(idx, weight)
    elif kernel_name == 'DTMD':

        # T
        dA = A.to_dense()
        A2 = torch.sparse.mm(A, dA)
        T = sparse_dense_mul(A, A2)
        idx = T._indices() # 2,W
        weight = T._values() # W


        # M for each i
        N = A.shape[0]
        self_loops = sparse_self_loop_idx(N)  # 2, N
        weight_loops = torch.tensor([torch.max(weight[idx[0,:] == i]) for i in range(N)], dtype=torch.float)

        # concat
        idx = torch.cat((idx, self_loops), dim=1)  # 2, W + N
        weight = torch.cat((weight, weight_loops), dim=0)  # W + N

        # update degree!!
        deg = sparse_get_degree_with_weight(idx, weight)

        # DxD normalization
        weight = sparse_DxD_normalization(idx, deg, weight)

        # convert to sparse matrix
        K = torch.sparse.FloatTensor(idx, weight)
    elif kernel_name == 'TM':

        # T
        dA = A.to_dense()
        A2 = torch.sparse.mm(A, dA)
        T = sparse_dense_mul(A, A2)
        idx = T._indices() # 2,W
        weight = T._values() # W


        # M for each i
        N = A.shape[0]
        self_loops = sparse_self_loop_idx(N)  # 2, N
        weight_loops = torch.tensor([torch.max(weight[idx[0,:] == i]) for i in range(N)], dtype=torch.float)

        # concat
        idx = torch.cat((idx, self_loops), dim=1)  # 2, W + N
        weight = torch.cat((weight, weight_loops), dim=0)  # W + N

        # convert to sparse matrix
        K = torch.sparse.FloatTensor(idx, weight)
    # elif kernel_name == 'DtTD':
    #     # weight on Triangle
    #     # difficult...
    #     # use dense....
    #
    #     dA = A.to_dense()
    #     A2 = torch.sparse.mm(A, dA)
    #     T = sparse_dense_mul(A, A2)
    #     idx = T._indices()
    #     weight = T._values()
    #
    #     N = A.shape[0]
    #
    #     # loop
    #     self_loops = sparse_self_loop_idx(N)  # 2, N
    #     weight_loops = torch.ones(N)  # N
    #
    #     # concat
    #     idx = torch.cat((idx, self_loops), dim=1)  # 2, W + N
    #     weight = torch.cat((weight, weight_loops), dim=0)  # W + N
    #
    #     # update degree!!
    #     deg = sparse_get_degree_with_weight(idx, weight)
    #
    #     # DxD normalization
    #     weight = sparse_DxD_normalization(idx, deg, weight)
    #
    #     # convert to sparse matrix
    #     K = torch.sparse.FloatTensor(idx, weight)
    elif kernel_name == 'DLD':
        # - A
        N = A.shape[0]
        idx: torch.Tensor = A._indices()  # 2, W
        deg = sparse_get_degree(idx)
        weight = torch.ones(idx.shape[1], dtype=torch.float) * (-1)  # W

        # D
        self_loops = sparse_self_loop_idx(N)  # 2, N
        weight_loops = deg.float()

        # D - A
        idx = torch.cat((idx, self_loops), dim=1)  # 2, W +
        weight = torch.cat((weight, weight_loops), dim=0)  # W + N

        # DxD normalization
        weight = sparse_DxD_normalization(idx, deg, weight)

        # convert to sparse matrix
        K = torch.sparse.FloatTensor(idx, weight)
    else:
        raise NotImplementedError

    return K


def Dx_normalization(D1, K):
    """
    :param D1: N
    :param K:
    :return:
    """
    D = torch.reciprocal(D1)
    D[D1 == 0] = 1
    D = D.unsqueeze(1)
    K = D * K
    return K


def DxD_normalization(D1, K):
    """
    :param D1: N
    :param K:
    :return:
    """
    D = torch.reciprocal(torch.sqrt(D1))
    D[D1 == 0] = 1
    D_row = D.unsqueeze(1)
    D_col = D.unsqueeze(0)
    K = D_row * K * D_col
    return K


def generate_kernel(A: torch.Tensor, kernel_name) -> torch.Tensor:
    if is_sparse(A):
        print("Please Use sparse")
        return generate_sparse_kernel(A, kernel_name)

    N = A.shape[0]
    I = torch.eye(N, dtype=torch.float)
    ds = torch.sum(A, dim=1)
    D = torch.diag(ds).float()
    L = D - A
    T = torch.matmul(A, A) * A

    if kernel_name == 'I':
        return I
    elif kernel_name == 'A':
        return A
    elif kernel_name == 'tA':
        return A + I
    elif kernel_name == 'tT':
        return T + I
    elif kernel_name == 'tTs':
        return torch.clamp_max( T + I, 1)
    elif kernel_name == 'L':
        return L
    elif kernel_name == 'DA':
        return Dx_normalization(ds, A)
    elif kernel_name == 'DtA':
        return Dx_normalization(ds + 1, A + I)
    elif kernel_name == 'DtT':
        DT1 = torch.sum(T, dim=1)
        return Dx_normalization(DT1 + 1, T + I)
    elif kernel_name == 'DL':
        return Dx_normalization(ds, L)
    elif kernel_name == 'DAD':
        return DxD_normalization(ds, A)
    elif kernel_name == 'DtAD':
        return DxD_normalization(ds + 1, A + I)
    elif kernel_name == 'TM':
        M = torch.diag(torch.max(T,dim=1)[0])
        TM = T + M
        return TM
    elif kernel_name == 'DTMD':
        M = torch.diag(torch.max(T,dim=1)[0])
        TM = T + M
        D1Dtm = torch.sum(TM, dim=1)
        K = DxD_normalization(D1Dtm, TM)
        return K
    # elif kernel_name == 'DtTD':
    #     DT1 = torch.sum(T, dim=1)
    #     return DxD_normalization(DT1 + 1, T + I)
    elif kernel_name == 'DLD':
        return DxD_normalization(ds, L)
    raise NotImplementedError


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--name', type=str, default='DtAD', help='')
    args = parser.parse_args()
    print(args)
    A = [
        [0, 1, 1, 1, 1, 0],
        [1, 0, 1, 0, 0, 0],
        [1, 1, 0, 1, 0, 0],
        [1, 0, 1, 0, 1, 1],
        [1, 0, 0, 1, 0, 0],
        [0, 0, 0, 1, 0, 0],
    ]

    A = torch.tensor(A, dtype=torch.float)
    sA = A.to_sparse()
    name = args.name
    K = generate_kernel(A, name)
    sK = generate_sparse_kernel(sA, name).to_dense()
    print(generate_kernel(A, 'A'))
    print(K)
    print(sK)

    assert torch.all(K == sK), 'err'
    print("OK")
