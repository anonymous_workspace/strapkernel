
import math
import sys
import os
import glob
import time
import argparse
import networkx as nx
import numpy as np
import pandas as pd
from datetime import datetime
import torch
import optuna

torch.set_printoptions(linewidth=200)

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler

#from vis_kernel import vis_kernel_on_designed, vis_graph
from tqdm import tqdm

from sklearn.base import BaseEstimator, clone
from sklearn.model_selection import GridSearchCV, cross_val_score, KFold, StratifiedKFold, BaseCrossValidator, \
    ParameterGrid

#import mrl.env as env
from util.u_time import time2str
#import motif.rooted_motiflist as rooted_motiflist
from model.StrapNCWrap import StrapNCWrap
from model.FixedNCWrap import FixedNCWrap
from model.StrapNCWrap import StrapNCWrap

from model.SuperNCWrap import SuperNCWrap

from data.Data import Data


class ShowTrial():
    def __init__(self):
        super().__init__()
    def suggest_loguniform(self, label, x,y):
        return "%.2e"%x + " -- " + "%.2e"%y
    def suggest_uniform(self, label, x, y):
        return str(x) + " -- " + str(y)
    def suggest_categorical(self,label:str, xs):
        return str(xs)


def NCcross_val_score(cv_wrap:SuperNCWrap, data:Data, niter: int):
    scores = []
    run_times = []
    buf_train_data = cv_wrap.buf_train_data
    for i in tqdm(range(niter)):
        data.update_mask(seed=i + 100) # fixed for fair comparison
        start = time.time()
        # clone
        estimator:SuperNCWrap = clone(cv_wrap)
        estimator.buf_train_data = buf_train_data
        # fit
        estimator.fit(data)
        score = estimator.score(data)
        scores.append(score)
        run_times.append(time.time() - start)
    print("average run time:%.3f[s]" % (np.average(run_times)))
    return scores



class NCGridSearch():
    def __init__(self, model, search_params, cv):
        super().__init__()
        self.model = model
        self.search_params = search_params
        self.cv = cv

        self.cv_results_ = {
            'params': [],
            'dif_params': [],
            'mean_test_score': [],
            'std_test_score': [],
            'rank_test_score': [],
            'mean_run_time': []
        }

        for i in range(cv):
            self.cv_results_['score_' + str(i)] = []
        self.best_params_ = {}
        self.best_score_ = 0

    def fit(self, data):

        search_params = self.search_params
        param_combinations = list(ParameterGrid(search_params))
        num_comb = len(param_combinations)

        print("Fitting %d times for each of %d candidates, totalling %d fits" % (self.cv, num_comb, self.cv * num_comb))
        for pi in tqdm(range(num_comb)):
            params = param_combinations[pi]
            self.cv_results_['params'].append(params)

            dif_paras = {}
            for key in search_params:
                if len(search_params[key]) == 1:
                    continue
                dif_paras[key] = params[key]
            self.cv_results_['dif_params'].append(dif_paras)

            scores = []
            run_times = []
            for i in range(self.cv):
                data.update_mask(seed=i + 1000)
                start = time.time()
                estimator = clone(self.model)
                estimator.set_params(**params)
                estimator.fit_with_valid(data, verbose=0)
                score = estimator.score(data)
                scores.append(score)
                run_times.append(time.time() - start)

                self.cv_results_['score_' + str(i)].append(score)

            u = np.average(scores)
            s = np.std(scores)

            self.cv_results_['mean_test_score'].append(u)
            self.cv_results_['std_test_score'].append(s)
            self.cv_results_['mean_run_time'].append(np.average(run_times))

            # u_scores.append(u)

        u_scores = np.array(self.cv_results_['mean_test_score'])
        y = np.flip(np.sort(u_scores))
        rank = [np.where(y == a)[0][0] + 1 for a in u_scores]

        self.cv_results_['rank_test_score'] = rank

        best_i = int(np.argmax(u_scores))
        self.best_params_ = self.cv_results_['params'][best_i]
        self.best_score_ = self.cv_results_['mean_test_score'][best_i]
