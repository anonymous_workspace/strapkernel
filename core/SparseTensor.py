
import torch

def is_sparse(x: torch.Tensor) -> bool:
    """
    :param x:
    :return: True if x is sparse tensor else False
    """
    try:
        x._indices()
    except RuntimeError:
        return False
    return True

class SparseTensor(torch.Tensor):
    """
    NeverUse
    """
    def __init__(self):
        super().__init__()
        raise NotImplementedError
    #
    # def _indices(self):
    #     raise NotImplementedError
    #
    # def _values(self):
    #     raise NotImplementedError