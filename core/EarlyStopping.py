
import math
import torch.nn as nn
from copy import deepcopy

# refrence https://github.com/Bjarten/early-stopping-pytorch/blob/master/pytorchtools.py

class EarlyStopper():
    def __init__(self, patience, trigger='loss', stopper='loss'):

        self.min_delta = 0
        self.patience = patience
        self.waiting = 0

        self.trigger = trigger
        self.stopper = stopper

        self.best_state = None

        # buf
        self.best_trigger = -math.inf
        self.best_stopper = -math.inf

        # flag
        # check this form outside
        self.stop_flag = False

        self.stop_signal = False
        self.trig_signal = False

    def set(self, model:nn.Module, val_loss:float, val_acc:float, verbose=0):

        val_loss = float(val_loss)
        val_acc = float(val_acc)

        v_trig = - val_loss if self.trigger == 'loss' else val_acc
        v_stop = - val_loss if self.stopper == 'loss' else val_acc

        reset = False

        self.stop_signal = False
        self.trig_signal = False

        if v_trig > self.best_trigger:
            self.best_state = deepcopy(model.state_dict())
            self.best_trigger = v_trig
            self.trig_signal = True
            reset = True
            if verbose > 0:
                print('trigger:', v_trig)

        if v_stop > self.best_stopper:
            self.best_stopper = v_stop
            self.stop_signal = True
            reset = True
            if verbose > 0:
                print('stopper:', v_stop)

        if reset:
            self.waiting = 0
        else:
            self.waiting += 1
            if self.patience > 0 and self.waiting > self.patience:
                # load automatically
                # print('STOP', self.waiting, self.patience)
                # model.load_state_dict(self.best_state)
                self.stop_flag = True

    def load_state(self, model):
        model.load_state_dict(self.best_state)
        return model




    @property
    def stop(self):
        return self.stop_flag

#
#
# class Trigger():
#
#     def __init__(self, min_delta = 0):
#
#         self.__min_delta = min_delta
#
#         self.best_score = -math.inf
#
#         self.is_best = False
#
#
#     def __call__(self, valid_score):
#         if valid_score > self.best_score + self.__min_delta:
#             self.is_best = True
#             self.best_score = valid_score
#         elif valid_score == self.best_score:
#             self.is_best = True
#         else:
#             self.is_best = False
#
# class EarlyStopping():
#     """
#     Call each time checking validation set
#     higher valid_score is better
#     """
#     def __init__(self, patience = 10, min_delta = 0):
#         if patience < 0:
#             patience = math.inf
#         self.__patience = patience
#         self.__min_delta = min_delta
#
#         self.__waiting = 0
#         self.best_score = - math.inf
#
#         # access from outside
#         self.early_stop = False
#         self.is_best = False
#
#     def __call__(self, valid_score):
#
#         if valid_score > self.best_score + self.__min_delta:
#             self.is_best = True
#             self.__waiting = 0
#
#             self.best_score = valid_score
#         elif valid_score == self.best_score:
#             self.is_best = True
#             self.__waiting = 0
#         else:
#             self.is_best = False
#             self.__waiting = self.__waiting + 1
#
#         if self.__waiting >= self.__patience:
#             self.early_stop = True
#         else:
#             self.early_stop = False
#
#
#


