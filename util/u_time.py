

def time2str(sec:float):
    #sec = int(sec+0.5)
    txt = ''
    if sec > 0:
        s = sec % 60
        sec = sec // 60
        txt = '%5.2f[s]'%(s)
    if sec > 0:
        m = sec % 60
        sec = sec // 60
        txt = '%2d[m]'%(m) + txt
    if sec > 0:
        h = sec % 24
        sec = sec // 24
        txt = '%2d[h]'%(h) + txt
    #print(sec)
    if sec > 0:
        d = sec
        txt = '%2d[d]'%(d) + txt

    return txt




