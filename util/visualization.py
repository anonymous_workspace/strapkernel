import math
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.patches as pat
from matplotlib import cm

from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA

ax_i = None
fig_i = None




# def vis_2(x, y):
#     fig = plt.figure(figsize=(12, 6))
#     ax = fig.add_subplot(1, 2, 1)
#
#     ax = fig.add_subplot(1, 2, 2)
#
#     plt.tight_layout()
#     plt.show()
#     return fig


def vis_2(f1, f2):
    # vis_2(lambda ax: ax_graph(ax, G),lambda ax: ax_cdf(ax, ds))
    fig = plt.figure(figsize=(12, 6))
    ax = fig.add_subplot(1, 2, 1)
    f1(ax)
    ax = fig.add_subplot(1, 2, 2)
    f2(ax)
    plt.tight_layout()
    plt.show()
    return fig


"""
HIgh Dimensional data
"""


def vis_hid_colored_i(data, labels, method='PCA'):
    
    global fig_i
    global ax_i
    if fig_i is None:
        fig_i, ax_i = plt.subplots()
    ax_i.clear()

    if data.shape[1] == 2:
        X_reduced = data
    elif method == 'PCA':
        X_reduced = PCA(n_components=2).fit_transform(data)
    elif method == 'TSNE':
        X_reduced = TSNE(n_components=2).fit_transform(data)
    else:
        raise ValueError('method error')

    x = X_reduced[:,0]
    y = X_reduced[:,1]

    ax = ax_i
    fig = fig_i

    colors = ['red', 'blue', 'green', 'grey', 'aqua', 'tomato', 'purple', 'gold', 'lime', 'sienna', 'lightpink']

    idx = np.unique(labels)
    # for g in np.unique(labels):
    for i in range(len(idx)):
        g = idx[i]
        ix = np.where(labels == g)
        l = g
        ax.scatter(x[ix], y[ix], c=colors[i], label=l, s=10)

    ax.legend()

    # cm.jet winter
    # cm.coolwarm
    # cm.inferno
    # https://matplotlib.org/examples/color/colormaps_reference.html
    # cm.hot
    #sc = ax.scatter(x, y, c=labels,cmap=cm.gist_rainbow)
    # sc = plt.scatter(xy, xy, c=z, vmin=0, vmax=20, s=35, cmap=cm)
    #plt.colorbar(sc)
    #ax.set_xlim(-1, 1)
    #ax.set_ylim(-1, 1)
    fig.tight_layout()
    plt.pause(0.01)

def vis_hid_colored(data, labels, method='PCA', label_names=None):

    if data.shape[1] == 2:
        X_reduced = data
    elif method == 'PCA':
        X_reduced = PCA(n_components=2).fit_transform(data)
    elif method == 'TSNE':
        X_reduced = TSNE(n_components=2).fit_transform(data)
    elif method == 'LDA':
        # normalization
        u = np.mean(data,axis=0)
        s = np.std(data - u, axis=0) + 1e-10
        data = (data- u)/s
        #clf = LDA(n_components=2)
        #clf.fit(data,labels)
        #X_reduced = clf.transform(data)
        X_reduced = LDA(n_components=2).fit_transform(data, labels)
        print(X_reduced.shape)
    else:
        raise ValueError('method error')

    x = X_reduced[:,0]
    y = X_reduced[:,1]

    fig = plt.figure(figsize=(5, 5))
    ax = fig.gca()

    colors = ['red', 'blue', 'green', 'grey', 'aqua', 'tomato', 'purple', 'gold', 'lime', 'sienna', 'lightpink']

    idx = np.unique(labels)
    #for g in np.unique(labels):
    for i in range(len(idx)):
        g = idx[i]
        ix = np.where(labels == g)
        l = g
        if label_names is not None:
            l = label_names[g]
        ax.scatter(x[ix], y[ix], c=colors[i], label=l, s=10)

    ax.legend()


    # cm.jet winter
    # cm.coolwarm
    # cm.inferno
    # https://matplotlib.org/examples/color/colormaps_reference.html
    # cm.hot
    # sc = ax.scatter(x, y, c=labels,cmap=cm.gist_rainbow)
    # sc = plt.scatter(xy, xy, c=z, vmin=0, vmax=20, s=35, cmap=cm)
    # plt.colorbar(sc)
    #ax.set_xlim(-1, 1)
    #ax.set_ylim(-1, 1)
    fig.tight_layout()
    plt.show()
    return fig


def vis_hid(data, method='PCA'):
    if data.shape[1] == 2:
        X_reduced = data
    elif method == 'PCA':
        X_reduced = PCA(n_components=2).fit_transform(data)
    elif method == 'TSNE':
        X_reduced = TSNE(n_components=2).fit_transform(data)
    else:
        raise ValueError('method error')
    vis_scatter(X_reduced[:,0],X_reduced[:,1])
#
# def vis_tsne(data):
#     X_reduced = TSNE(n_components=2).fit_transform(data)
#     vis_scatter(X_reduced[:, 0], X_reduced[:, 1])


def vis_plot(x, y):
    fig = plt.figure(figsize=(5, 5))
    ax = fig.gca()

    ax.plot(x, y, 'o-')
    # ax.set_ylim(min(y),min(y)*20)
    fig.tight_layout()
    plt.show()


"""
scatter
"""

def ax_scatter(ax, xy, y=None):
    if y is None:
        x = xy[:,0]
        y = xy[:,1]
    else:
        x = xy
    ax.scatter(x, y)
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)


def vis_scatter(x, y):
    fig = plt.figure(figsize=(5, 5))
    ax = fig.gca()
    ax_scatter(ax,x,y)
    #ax.set_xlim(-1, 1)
    #ax.set_ylim(-1, 1)
    fig.tight_layout()
    plt.show()


def vis_scatter_i(x, y):
    global fig_i
    global ax_i
    if fig_i is None:
        fig_i, ax_i = plt.subplots()
    ax_i.clear()

    ax_scatter(ax_i,x,y)
    fig_i.tight_layout()
    plt.pause(0.01)


def vis_scatter2_i(x1, y1, x2, y2, rs):
    global fig_i
    global ax_i
    if fig_i is None:
        fig_i, ax_i = plt.subplots()

    ax_i.clear()
    ax_i.scatter(x1, y1)
    ax_i.scatter(x2, y2, c='r')

    for i in range(len(x2)):
        # circle = plt.Circle((x2[i], y2[i]), rs[i], color='r', fill=False)
        # ax_i.add_artist(circle)
        e1 = pat.Ellipse((x2[i], y2[i]), width=rs[0, i], height=rs[1, i], fill=False, color="red")
        ax_i.add_patch(e1)

    ax_i.set_xlim(-1, 1)
    ax_i.set_ylim(-1, 1)
    fig_i.tight_layout()
    plt.pause(0.01)



"""

"""

def ax_cdf(ax, x):

    x = np.array(x)
    x = np.sort(x)
    num_x = len(x)
    ny = [1]
    nx = [min(x)]

    prev_x = None
    for i in range(num_x):
        t = x[i]
        if prev_x == t:
            continue

        nx.append(t)
        ny.append(len(x[x >= t])/ num_x)

        prev_x = t

    e = 1e-10
    nx.append(np.max(x) + e)
    ny.append(0)

    ax.plot(nx,ny, 'o-')



def vis_cdf(x):
    fig = plt.figure(figsize=(5, 5))
    ax = fig.gca()

    ax_cdf(ax,x)

    fig.tight_layout()
    plt.show()
    return fig


"""
Histgrapm
"""


def ax_hist(ax, x):
    #ax.hist(x, bins=50)
    N = x.shape[0]
    n_bins = 40
    hist, bins = np.histogram(x,bins=n_bins, range=(min(x), max(x)))

    # print(hist.shape)
    # print(bins.shape)
    # print(hist)
    # print(bins)

    y = hist
    #x = bins[:-1]
    x = [(bins[i] + bins[i+1])/2 for i in range(len(bins)-1)]
    x = np.array(x)

    w = (max(x) - min(x))/40
    ax.bar(x,y/N,width=w) #
    #exit()
    #ax.set_xlim(0,2)
    #ax.set_ylim(0,0.10)


def vis_hist_i(x):
    global fig_i
    global ax_i
    if fig_i is None:
        fig_i, ax_i = plt.subplots()

    ax_i.clear()


    ax_hist(ax_i,x)
    #ax_i.set_xlim(-1, 1)
    fig_i.tight_layout()
    plt.pause(0.01)


def vis_hist(x):
    fig = plt.figure(figsize=(5, 5))
    ax = fig.gca()

    ax_hist(ax,x)

    fig.tight_layout()
    plt.show()
    return fig


"""
Graph
"""


def ax_graph(ax, A):
    """
    visualize graph and show window
    A is nx.Graph or numpy matrix
    """

    if isinstance(A, type(nx.Graph())):
        G = A
        N = len(G)
    else:
        G = nx.from_numpy_matrix(A)
        N = A.shape[0]

    options = {
        'edge_color': 'black',
        'width': 1,
        'with_labels': True,  # [str(i) for i in range(N)],
        'font_weight': 'regular',
        'node_size': 800,
        'font_size': 20,
        'node_color': 'orange',
    }

    nx.draw(G, pos=nx.spring_layout(G, k=1, iterations=200), ax=ax, **options)


def vis_graph_color(A, color_indices):
    """
    visualize graph and show window
    A is nx.Graph or numpy matrix
    """
    fig = plt.figure(figsize=(8, 6))
    ax = fig.gca()

    if isinstance(A, type(nx.Graph())):
        G = A
        N = len(G)
    else:
        G = nx.from_numpy_matrix(A)
        N = A.shape[0]

    colors = ['red','blue','green','grey','aqua','tomato','purple','sage']

    color_map = []
    for i in range(len(nx.nodes(G))):
        #node = list(nx.nodes(G))[i]
        #print(node)
        #color_map.append(colors[color_indices[i]])
        color_map.append(colors[color_indices[i]])
    #print(color_map)

    options = {
        'edge_color': 'black',
        'width': 1,
        'with_labels': True,  # [str(i) for i in range(N)],
        'font_weight': 'regular',
        'node_size': 800,
        'font_size': 20,
        'node_color': color_map,
    }

    nx.draw(G, pos=nx.spring_layout(G, k=0.5, iterations=200), ax=ax, **options)
    plt.show()
    return fig



def vis_graph(A):
    fig = plt.figure(figsize=(8, 6))
    ax = fig.gca()
    ax_graph(ax, A)
    plt.show()
    return fig


def vis_2graphs(A1, A2):
    fig = plt.figure(figsize=(12, 6))
    ax = fig.add_subplot(1, 2, 1)
    ax_graph(ax, A1)
    ax = fig.add_subplot(1, 2, 2)
    ax_graph(ax, A2)
    plt.tight_layout()
    plt.show()
    return fig


"""
Heatmaps
"""


def ax_heatmap(ax, data):
    W, H = data.shape  # len(x), len(y)

    # # 0,1
    im = ax.imshow(data, interpolation='nearest', cmap=cm.copper)
    im.set_clim(0, 1)

    # -1,1
    # im = ax.imshow(data, interpolation='nearest', cmap=cm.bwr)
    # im.set_clim(-1, 1)

    # cbar = fig.colorbar(im, ticks=[np.min(data), 0, np.max(data)])
    # cbar = fig.colorbar(im, ticks=[0, 0.2,0.4,0.6,0.8, 1])

    # cm.jet winter
    # cm.coolwarm
    # cm.inferno
    # https://matplotlib.org/examples/color/colormaps_reference.html
    # cm.hot

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    # show value
    if max(W, H) < 20:
        for i in range(H):
            for j in range(W):
                ax.text(j, i, "%.2f" % data[i, j], ha="center", va="center", color="w")



def vis_confusion(y_true, y_pred):
    from sklearn.metrics import confusion_matrix, accuracy_score

    cmx_data = confusion_matrix(y_true, y_pred)
    csum = np.sum(cmx_data, axis=1)
    csum[csum == 0] = 1
    cmx_data = cmx_data / np.expand_dims(csum, axis=1)

    vis_heatmap(cmx_data)

def vis_heatmap(data):
    fig = plt.figure(figsize=(5, 5))
    ax = fig.gca()
    ax_heatmap(ax, data)
    fig.tight_layout()
    plt.show()


def vis_2heatmap(data1, data2):
    fig = plt.figure(figsize=(12, 6))
    ax = fig.add_subplot(1, 2, 1)
    ax_heatmap(ax, data1)
    ax = fig.add_subplot(1, 2, 2)
    ax_heatmap(ax, data2)

    fig.tight_layout()
    plt.show()


def vis_heatmap_i(data):
    global fig_i
    global ax_i
    if fig_i is None:
        fig_i, ax_i = plt.subplots()

    ax_i.clear()
    ax_heatmap(ax_i, data)

    fig_i.tight_layout()
    plt.pause(0.001)


"""
xyz
"""


def vis_xyz(x, y, z):
    """
    :param x: 100
    :param y: 150
    :param z: 100 * 150
    :return:
    """
    # x = np.arange(0, 10, 0.05)
    # y = np.arange(0, 10, 0.05)

    X, Y = np.meshgrid(x, y)
    # Z = np.sin(X) + np.cos(Y)

    fig, ax = plt.subplots()
    # im = ax.pcolormesh(X, Y, z, cmap='viridis')
    im = ax.contour(X, Y, z, cmap='viridis')
    im.set_clim(np.min(z), np.max(z))

    pp = fig.colorbar(im, orientation="vertical")
    pp.set_label('z')

    ax.set_xlabel('x')
    ax.set_ylabel('y')

    plt.show()
    return fig


if __name__ == "__main__":
    vis_xyz(0, 0, 0)
