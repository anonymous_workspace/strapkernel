import time
import argparse
import numpy as np
import torch
import optuna

# print settings
np.set_printoptions(precision=4, floatmode='fixed', suppress=True, linewidth=200)
torch.set_printoptions(linewidth=200)

from tqdm import tqdm
from sklearn.base import clone

from util.u_time import time2str
import util.save_logs as save_logs

from core.nc_experiments import NCcross_val_score, ShowTrial

from model.ChebNCWrap import ChebNCWrap
from model.FChebNCWrap import FChebNCWrap
from model.FixedNCWrap import FixedNCWrap
from model.SuperNCWrap import SuperNCWrap
from model.StrapNCWrap import StrapNCWrap

from data.Data import Data
from best_params import best_param_log


def cv(cv_model, data, niter=10):
    start = time.time()
    scores = NCcross_val_score(cv_model, data, niter=niter)
    scores = np.array(scores)
    print(scores)

    print("ave:", np.average(scores))
    print("std:", np.std(scores))

    # print for latex
    print('$ %.2f \pm %.2f $' % (round(np.average(scores) * 100, 1), round(np.std(scores) * 100, 1)))

    print('all done:', time2str(time.time() - start))

    # save log
    fpath = save_logs.logger.get_filehead() + '_exp1_cv.csv'
    with open(fpath, 'w') as f:
        f.write('ave, %.4f\n' % (np.average(scores)))
        f.write('std, %.4f\n' % (np.std(scores)))
        for i in range(niter):
            f.write('score %3d, %.4f\n' % (i, scores[i]))
    exit()


def test(cv_model: SuperNCWrap, data: Data, data_name: str, n_cv=5, niter=10, loadparam=True):
    best_param = best_param_log(data_name, cv_model.kernel_name)
    if loadparam and best_param is not None:
        print('\nParam loaded')
        #print(best_param)
    else:
        best_param = gs_optuna(cv_model, data, n_cv=n_cv, n_try=30)
        #del best_param['df'], best_param['K'], best_param['n_epochs'], best_param['patience'], best_param['kernel_name'], best_param['psi_d']
        #print("\nparams['" + data_name + "']['" + cv_model.kernel_name + "'] = ", best_param)
    print()

    cv_model.set_params(**best_param)
    cv(cv_model, data, niter)
    exit()


def gs_optuna(wrap, data, n_cv=5, n_try=50):
    print('\nHyperparameter search by Optuna')
    start = time.time()
    optuna.logging.set_verbosity(optuna.logging.WARNING)
    base_param = {
        'psi_p': 1,
        'psi_d': 32,
        'patience': 100,
        'n_epochs': 200
    }
    buf_train_data = wrap.buf_train_data
    wrap = clone(wrap)
    wrap.set_params(**base_param)

    def objwrap(pbar):
        def objective(trial):
            if not isinstance(wrap, StrapNCWrap):
                search_param = {
                    'psi_do': trial.suggest_uniform('psi_do', 0.0, 0.8) if data.features.shape[1] > 1 else 0,
                    'lr': trial.suggest_loguniform('lr', 1e-3, 1e-0),
                    'L2': trial.suggest_loguniform('L2', 1e-4, 1e-0)
                }
            else:
                search_param = {
                    'phi_p': 2,
                    'phi_d': 16,
                    'psi_do': trial.suggest_uniform('psi_do', 0.0, 0.8) if data.features.shape[1] > 1 else 0,
                    'lr': trial.suggest_loguniform('lr', 1e-3, 1e0),
                    'L2': trial.suggest_loguniform('L2', 1e-4, 1e0),
                    'plus_only': trial.suggest_categorical('plus_only', [True, False]),
                    'allow_loop': True
                }

            # printing for log
            if isinstance(trial, ShowTrial):
                final_param = wrap.get_params()
                final_param.update(search_param)
                print("search_params:\n{")
                for key in final_param:
                    print("  '" + (key + "'").ljust(11, " ") + ":" + str(final_param[key]) + ",")
                print("}")
                return
            wrap.set_params(**search_param)

            scores = []
            for i in range(n_cv):
                data.update_mask(seed=None)
                estimator: SuperNCWrap = clone(wrap)
                estimator.set_params(**search_param)
                estimator.buf_train_data = buf_train_data
                estimator.fit(data)
                score = estimator.score(data)
                scores.append(score)
            pbar.update()
            return 1 - np.average(scores)

        return objective

    # print searching space
    objwrap(None)(ShowTrial())

    study = optuna.create_study()
    with tqdm(total=n_try) as pbar:
        study.optimize(objwrap(pbar), n_trials=n_try)

    hist_df = study.trials_dataframe()
    hist_df.to_csv(save_logs.logger.get_filehead() + '_exp1_opt.csv')

    print('\n')
    print('Search Result:', study.best_params)
    print('BestScore:', 1 - study.best_value)
    elapsed = time.time() - start
    print('all done :', time2str(elapsed), ' average:', time2str(elapsed/n_try))

    wrap.set_params(**study.best_params)
    best_param = wrap.get_params()

    print('BestParam :', best_param)
    return best_param


if __name__ == "__main__":
    save_logs.set_save_log(True)

    parser = argparse.ArgumentParser(description='This is an implementation of the experiments of StrapKernel')
    parser.add_argument('--dataset', type=str, default='cornell', help='')
    parser.add_argument('--mode', type=str, default='run', help='run, opt, cv, test')
    parser.add_argument('--niter', type=int, default=100, help='number of iteration')
    parser.add_argument('--kernel', type=str, default='I', help='convolutional kernel, I,A,tA,DA,DtAD,DTMD,C,S')
    parser.add_argument('--loadparam', action='store_true', default=False, help='load the best param if available')

    args = parser.parse_args()
    print(args)

    data = Data.load(args.dataset)
    K = data.num_classes
    df = data.num_features

    if args.kernel == 'S':
        wrap = StrapNCWrap(K, df)
    elif args.kernel == 'C':
        wrap = ChebNCWrap(K, df)
    elif args.kernel == 'FC':
        wrap = FChebNCWrap(K, df)
    else:
        wrap = FixedNCWrap(K, df)
    param = {
        'L': 2,
        'S': 1,
        'T': 1,
        'kernel_name': args.kernel,
        'phi_p': 2,
        'phi_d': 16,
        'psi_p': 1,
        'psi_d': 32,
        'psi_do': 0.0,
        'lr': 0.5,
        'L2': 0.01,
        'plus_only': True,
        'allow_loop': True,
        'patience': 100,
        'n_epochs': 200
    }

    wrap.set_params(**param)
    if args.loadparam:
        best_param = best_param_log(args.dataset, param['kernel_name'])
        if best_param is not None:
            wrap.set_params(**best_param)
            print("best param loaded:")
            print(best_param)
        else:
            print("best param: Not found")


    print("Mode:", args.mode.lower())
    wrap.precompute(data)
    if args.mode.lower() == "cv":
        print(wrap.get_params())
        cv(wrap, data, niter=args.niter)
        exit()
    elif args.mode == "opt":
        gs_optuna(wrap, data, n_cv=5, n_try=30)
        exit()
    elif args.mode == "test":
        test(wrap, data, data_name=args.dataset, niter=args.niter,loadparam=args.loadparam)
        exit()

    print("param = " + str(wrap.get_params()))

    data.update_mask(seed=None)
    wrap.fit_with_valid(data, verbose=2, save_file_head=save_logs.logger.get_filehead())
