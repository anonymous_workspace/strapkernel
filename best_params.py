def best_param_log(dataset, kernel) -> dict:
    #return None
    params = {}
    params['cornell'] = {}
    params['wisconsin'] = {}
    params['cora'] = {}
    params['citeseer'] = {}
    params['chameleon'] = {}
    params['squirrel'] = {}
    params['amazon-photo'] = {}
    params['amazon-computers'] = {}
    params['pubmed'] = {}

    params['cornell']['S'] = {'lr': 0.12127631628500939, 'L2': 0.02367782871606465,
                              'L': 2, 'S': 1, 'T': 1,
                              'phi_p': 2, 'phi_d': 16, 'psi_p': 1,
                              'psi_do': 0.4135898766004768, 'allow_loop': True, 'plus_only': True,
                              }
    params['wisconsin']['S'] = {'lr': 0.11688273050216416, 'L2': 0.029166336589327206,
                                'S': 1, 'T': 1, 'psi_p': 1,
                                'psi_do': 0.39711753764228525, 'L': 2, 'phi_p': 2, 'phi_d': 16,
                                'allow_loop': True, 'plus_only': False}
    params['cora']['S'] = {'lr': 0.1388621605925155, 'L2': 0.005624304571888493,
                           'L': 2, 'S': 1, 'T': 1,
                           'phi_p': 2, 'phi_d': 16, 'psi_p': 1,
                           'psi_do': 0.10705695333828848, 'allow_loop': True, 'plus_only': True,
                           }
    params['citeseer']['S'] = {'lr': 0.8696748877553544, 'L2': 0.01947191358784822,
                               'L': 2, 'S': 1, 'T': 1,
                               'phi_p': 2, 'phi_d': 16, 'psi_p': 1,
                               'psi_do': 0.33217777143936483, 'allow_loop': True, 'plus_only': True,
                               }
    params['chameleon']['S'] = {'lr': 0.049142520895228116, 'L2': 0.005432536729458178,
                                'L': 2, 'S': 1, 'T': 1,
                                'phi_p': 2, 'phi_d': 16, 'psi_p': 1,
                                'psi_do': 0.35885940041822056, 'allow_loop': True, 'plus_only': True,
                                }
    params['squirrel']['S'] = {'lr': 0.006682350975949748, 'L2': 0.006014293018996396,
                               'L': 2, 'S': 1, 'T': 1,
                               'phi_p': 2, 'phi_d': 16, 'psi_p': 1,
                               'psi_do': 0.1600667501960898, 'allow_loop': True, 'plus_only': True,
                               }
    params['amazon-photo']['S'] = {'lr': 0.015218299012204518, 'L2': 0.0008341653457183388,
                                   'L': 2, 'S': 1, 'T': 1,
                                   'phi_p': 2, 'phi_d': 16, 'psi_p': 1,
                                   'psi_do': 0.2297653041904974, 'allow_loop': True, 'plus_only': True,
                                   }
    params['amazon-computers']['S'] = {'lr': 0.06170748439745428, 'L2': 0.0044270150971638745,
                                       'L': 2, 'S': 1, 'T': 1,
                                       'phi_p': 2, 'phi_d': 16, 'psi_p': 1,
                                       'psi_do': 0.21323449077864734, 'allow_loop': True, 'plus_only': True,
                                       }
    params['pubmed']['S'] = {'kernel_name': 'S', 'lr': 0.02627282017648653, 'L2': 0.0002199472191340485,
                             'L': 2, 'S': 1, 'T': 1,
                             'phi_p': 2, 'phi_d': 16,'psi_p': 1,
                             'psi_do': 0.18160074617764202, 'allow_loop': True, 'plus_only': False}
    #
    if not dataset in params:
        return None
    if not kernel in params[dataset]:
        return None
    return params[dataset][kernel]
