

import argparse
import torch
import networkx as nx
import numpy as np

from data.Data import Data
from core.graph_util import generate_kernel


def BA(N, m=2,seed=None):
    while True:
        G = nx.barabasi_albert_graph(n=N, m=m,seed=seed)
        if seed is not None:
            assert nx.is_connected(G), 'G is not connected'
            return G
        elif nx.is_connected(G):
            return G

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--kernel', type=str, default='I', help='')
    #parser.add_argument('--L', type=int, default=1, help='')
    parser.add_argument('--seed', type=int, default=None, help='')
    args = parser.parse_args()
    print(args)

    # generate synthesized data set
    np.random.seed(seed=args.seed)

    N = 1000
    m = 5

    G = BA(N, m,seed=args.seed)

    # doubling edgelist
    edge_list = torch.tensor(list(nx.edges(G)), dtype= torch.long).transpose(1, 0)
    directed = torch.stack((edge_list[1], edge_list[0]), dim=0)
    edge_list = torch.cat((edge_list, directed), dim=1)

    # randomly set node feature
    features = torch.zeros((N, 1), dtype=torch.float)
    idx = np.random.permutation(np.arange(N))[:int(N/2)]
    features[idx, :] = 1.0

    # generate kernel
    A = torch.tensor(nx.to_numpy_matrix(G), dtype=torch.float)
    K = generate_kernel(A, args.kernel)

    # powered kernel
    # L = args.L
    # Kp = K
    # for _ in range(1,L):
    #     Kp = torch.matmul(Kp,K)
    # K = Kp

    # convolution
    Z = torch.matmul(K, features).float() # N,1
    Z = Z[:,0] # N

    # th should be decided automatically
    zdes = np.sort(Z.numpy(),axis=0)

    th = zdes[int(zdes.shape[0]/2)]
    print('threshold:', th)

    x = Z.numpy()
    labels = torch.zeros((N), dtype=torch.long)
    labels[Z > th] = 1

    dt = Data(edge_list, features, labels)
    dt.print_statisitcs()
    dt.save('synthetic/generated-' + args.kernelc) #+ str(L)


