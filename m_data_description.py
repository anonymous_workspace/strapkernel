

import os
import argparse

from data.Data import Data

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='print statistics')
    parser.add_argument('--dataset', type=str, default='data-A1',help='data set name')
    args = parser.parse_args()

    if args.dataset == 'all':
        for d1 in ['synthetic','realworld']:
            for d2 in os.listdir('data/' + d1):
                data = Data.load(d2)
                data.print_statisitcs()
        exit()

    data = Data.load(args.dataset)
    data.print_statisitcs()