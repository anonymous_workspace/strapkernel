# Strap Kernel
A novel kernel-learning graph convolutional kernel.

## Requirement
- Python 3.6
- PyTorch 1.1
- scikit-learn 0.21
- NumPy 1.17
- Networkx 2.4
- Optuna 1.2

etc.

## Usage
### Data statistics
```commandline
python3 m_data_description.py --dataset all
```

### Generate synthetic dataset
```commandline
python3 m_generate_synthetic_dataset.py --kernel DtAD --seed 100
```


### Experiments
```commandline
python3 exp1_node_classification.py --dataset data-A --niter 100 --mode test --kernel A
python3 exp1_node_classification.py --dataset cora --mode run --kernel S --loadparam
```
Parameters:

dataset

- Synthetic data: data-A, -tA, -DA, -DtAD, -DTMD
- Real world data: cornell, wisonsin, cora, citeseer, chameleon, squirrel, amazon-photo, amazon-computers, pubmed

kernel

- A: A
- tA: I + A
- DA: D^{-1}A
- DtAD: D^{-1/2}(I + A)D^{-1/2}
- DTMD: D^{-1/2}(T + M)D^{-1/2} 
- C: ChebyNet (= FC is the fast variant)
- S: StrapKernel

mode

- run: just run experiment with fixed parameters.
- cv: run experiments [--niter] times with fixed parameters.
- opt: search hyperparameters using Optuna with 30 trials.
- test: run total experiment; first run "opt" and then "cv". The experiments on the paper is done with this.

niter

- number of runs for cv and test.

loadparam

- to load hyperparameters used in the paper. Only for StrapKernel. 

## License
MIT

## Citation
Under review.