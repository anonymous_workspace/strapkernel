from model.SuperNCWrap import SuperNCWrap
from model.StrapNCModel import StrapNCModel


class StrapNCWrap(SuperNCWrap):

    def __init__(self, K: int, df: int, **parameters):
        self.K = K
        self.df = df
        self.S = 1
        self.T = 1
        self.psi_p = 1
        self.psi_d = 16
        self.psi_do = 0.2
        self.L = 2
        self.phi_p = 2
        self.phi_d = 16
        self.allow_loop = True
        self.plus_only = False
        super().__init__()
        self.set_params(**parameters)

    def get_params(self, deep=True):
        prms = super().get_params(deep)
        prms["K"] = self.K
        prms["df"] = self.df
        prms["S"] = self.S
        prms["T"] = self.T
        prms["psi_p"] = self.psi_p
        prms["psi_d"] = self.psi_d
        prms["psi_do"] = self.psi_do
        prms["L"] = self.L
        prms["phi_p"] = self.phi_p
        prms["phi_d"] = self.phi_d
        prms["allow_loop"] = self.allow_loop
        prms["plus_only"] = self.plus_only
        return prms

    def set_params(self, **parameters):
        super().set_params(**parameters)
        self.model = StrapNCModel(self.K, self.df, L=self.L, S=self.S, T=self.T, phi_p=self.phi_p, phi_d=self.phi_d,
                                  psi_p=self.psi_p, psi_d=self.psi_d, psi_do=self.psi_do,
                                  allow_loop=self.allow_loop, plus_only=self.plus_only)
        return self
