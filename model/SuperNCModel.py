
import torch
import torch.nn as nn
import torch.nn.functional as F

class Mish(nn.Module):
    def forward(self, x):
        return x * torch.tanh(F.softplus(x))

def gen_psi(p, d_in, d_hid, d_out, do, nonlinear = False):
    top = []
    dc = d_in
    for i in range(p - 1):
        top.append(nn.BatchNorm1d(dc))
        top.append(nn.Dropout(do))
        top.append(nn.Linear(dc, d_hid))
        top.append(Mish())
        dc = d_hid

    top.append(nn.BatchNorm1d(dc))
    top.append(nn.Dropout(do))
    top.append(nn.Linear(dc, d_out))
    if nonlinear:
        top.append(Mish())
    psi = nn.Sequential(*top)
    return psi