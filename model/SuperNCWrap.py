import time

import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as F
from sklearn.metrics import accuracy_score

from core.EarlyStopping import EarlyStopper
from data.Data import Data
from util.u_time import time2str
from util.visualization import vis_confusion


def NC_accuracy_score(y: torch.Tensor, output: torch.Tensor, mask: torch.Tensor):
    y = y.numpy().astype(int)
    output = output.numpy().astype(int)
    mask = mask.numpy().astype(bool)
    y = y[mask]
    output = output[mask]
    return accuracy_score(y, output)


class NCLoss(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, output: torch.Tensor, y: torch.Tensor, mask: torch.Tensor):
        loss = F.nll_loss(output[mask], y[mask])
        return loss


class SuperNCWrap():

    def __init__(self, **parameters):
        super().__init__()
        self.model = None

        # set default values here
        self.lr = 0.01
        self.L2 = 1e-4
        self.n_epochs = 200
        self.patience = 100
        self.kernel_name = 'X'
        self.set_params(**parameters)

    def get_params(self, deep=True):
        prms = {}
        prms["kernel_name"] = self.kernel_name
        prms['lr'] = self.lr
        prms['L2'] = self.L2
        prms['n_epochs'] = self.n_epochs
        prms['patience'] = self.patience
        return prms

    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            setattr(self, parameter, value)
        return self

    def score(self, data: Data):
        glassfier = self.model
        glassfier.eval()

        train_data = self.model.convert_train(data)
        output_train = glassfier(*train_data)
        y_pred = torch.argmax(output_train.detach(), dim=1)
        acc_tests = NC_accuracy_score(data.labels, y_pred, data.tests_mask)
        return acc_tests

    def precompute(self, data: Data):
        print("data-precomputing")
        start = time.time()
        self.buf_train_data = self.model.convert_train(data)
        print('precomputing done:',time2str(time.time() - start))

    def fit(self, data: Data):
        glassfier = self.model
        n_epochs = self.n_epochs

        y: torch.Tensor = data.labels
        if self.buf_train_data is None:
            self.precompute(data)
        train_data = self.buf_train_data


        train_mask = data.train_mask
        valid_mask = data.valid_mask

        stopper = EarlyStopper(patience=self.patience, trigger='loss', stopper='loss')

        # =============================================================================
        # learning
        # =============================================================================

        criterion = NCLoss()  # nn.NLLLoss()
        d_optimizer = torch.optim.Adam(glassfier.parameters(), self.lr, weight_decay=self.L2)

        glassfier.train()

        for epoch in range(n_epochs + 1):
            output = glassfier(*train_data)
            d_loss = criterion(output, y, train_mask)
            d_optimizer.zero_grad()
            d_loss.backward()
            d_optimizer.step()

            with torch.no_grad():
                glassfier.eval()
                output_train = glassfier(*train_data)
                loss_valid = criterion(output_train, y, valid_mask).item()
                stopper.set(self.model, loss_valid, 0)

                if stopper.stop:
                    break
            glassfier.train()

        stopper.load_state(self.model)

        glassfier.eval()
        return self

    def fit_with_valid(self, data: Data, verbose: int = 1, save_file_head='logs/fit_with_valid'):
        glassfier = self.model
        n_epochs = self.n_epochs

        num_train = data.num_train
        num_valid = data.num_valid
        num_tests = data.num_tests

        if verbose > 0:
            print("train:", num_train, " valid:", num_valid, " test:", num_tests)

        y: torch.Tensor = data.labels
        if self.buf_train_data is None:
            self.precompute(data)
        train_data = self.buf_train_data

        train_mask = data.train_mask
        valid_mask = data.valid_mask
        tests_mask = data.tests_mask

        logs = []

        stopper = EarlyStopper(patience=self.patience, trigger='loss', stopper='loss')

        # =============================================================================
        # learning
        # =============================================================================

        criterion = NCLoss()  # nn.NLLLoss()
        d_optimizer = torch.optim.Adam(glassfier.parameters(), self.lr, weight_decay=self.L2)

        start = time.time()

        glassfier.train()

        for epoch in range(n_epochs + 1):

            output = glassfier(*train_data)
            d_loss = criterion(output, y, train_mask)
            d_optimizer.zero_grad()
            d_loss.backward()
            d_optimizer.step()

            with torch.no_grad():
                glassfier.eval()

                # train loss
                output_train = glassfier(*train_data)
                loss_train = criterion(output_train, y, train_mask).item()

                # train_acc
                y_pred = torch.argmax(output_train.detach(), dim=1)
                acc_train = NC_accuracy_score(y, y_pred, train_mask)

                # valid loss
                loss_valid = criterion(output_train, y, valid_mask).item()

                # valid_acc
                acc_valid = NC_accuracy_score(y, y_pred, valid_mask)

                # tests loss
                loss_tests = criterion(output_train, y, tests_mask).item()

                # tests_acc
                acc_tests = NC_accuracy_score(y, y_pred, tests_mask)

                elapse = time.time() - start
                estimated = elapse / (epoch + 1) * (n_epochs + 1)

                if verbose > 0:
                    print(
                        "%6d: train_loss:%10.4f, train_acc: %.4f, valid_loss:%10.4f, valid_acc: %.4f, test_acc: %.6f elapse:%s, estimated:%s" % (
                            epoch, loss_train, acc_train, loss_valid, acc_valid, acc_tests, time2str(elapse),
                            time2str(estimated)))

                stopper.set(self.model, loss_valid, acc_valid, verbose - 1)

                stop_i = int(stopper.stop_signal)
                trig_i = int(stopper.trig_signal)

                alog = [epoch, loss_train, loss_valid, loss_tests, acc_train, acc_valid, acc_tests, stop_i, trig_i]
                logs.append(alog)

                if stopper.stop:
                    break

            glassfier.train()

        stopper.load_state(self.model)

        glassfier.eval()

        if verbose < 1:
            return self

        # save log
        df = pd.DataFrame(logs)
        df.columns = ["epochs", "loss-train", "loss-valid", "loss-test", "acc-train", "acc-valid", "acc-test",
                      "sig-stop", "sig-trig"]
        df.set_index(df.columns[0], inplace=True)
        df.to_csv(save_file_head + "_exp4.csv")

        output_train = glassfier(*train_data)
        y_pred = torch.argmax(output_train.detach(), dim=1)
        acc_tests = NC_accuracy_score(y, y_pred, tests_mask)

        print("best test score:", acc_tests)
        vis_confusion(y[tests_mask], y_pred[tests_mask])

        return self

    # def save(self, path):
    #     torch.save(self.model.state_dict(), path)
    #
    # def load(self, path):
    #     self.model.load_state_dict(torch.load(path))
