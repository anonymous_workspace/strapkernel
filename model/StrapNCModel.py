# check whether gnc can predict motif counts for each node


import networkx as nx
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

from core.StrapKernel import StrapKernel
from core.SparseTensor import is_sparse, SparseTensor
from data.Data import Data

from copy import deepcopy

from model.SuperNCModel import gen_psi


class StrapNCModel(nn.Module):

    def __init__(self, K, df, **parameters):
        super().__init__()
        self.K = K
        self.df = df

        # save to self
        for parameter, value in parameters.items():
            setattr(self, parameter, value)

        kernels = []
        psis = []
        for i in range(self.T):
            d_in = df if i == 0 else self.psi_d
            d_out = K if i == self.T - 1 else self.psi_d
            nonlinear = False if i == self.T - 1 else True

            kernel = StrapKernel(self.L, self.S, phi_p=self.phi_p, phi_d=self.phi_d, allow_loop=self.allow_loop,
                                 plus_only=self.plus_only)
            psi = gen_psi(self.psi_p, d_in, self.psi_d, d_out, self.psi_do, nonlinear)

            kernels.append(kernel)
            psis.append(psi)

        # regeister as parameters
        self.ml_kernels = nn.ModuleList(kernels)
        self.kernels = kernels
        self.ml_psis = nn.ModuleList(psis)
        self.psis = psis

        # classifier
        self.top = nn.LogSoftmax(dim=1)

    def convert_train(self, data: Data) -> (torch.Tensor, torch.Tensor, torch.Tensor):
        X = data.features
        A = data.A.to_dense()
        D = StrapKernel.base_monomials(A, self.L)
        M = StrapKernel.mask_from_A(A, self.S, self.allow_loop)

        M: SparseTensor = M.to_sparse()

        idx = M._indices()  # 2,W
        valD = D[idx[0], idx[1], :]  # W,Q(L)
        return X, idx, valD

    def generate_kernel(self, A: torch.Tensor) -> torch.Tensor:
        with torch.no_grad():
            self.eval()
            D = StrapKernel.base_monomials(A, self.L)
            M = StrapKernel.mask_from_A(A, self.S, self.allow_loop)
            C: SparseTensor = M.to_sparse()
            idx = C._indices()  # 2, W
            valD = D[idx[0], idx[1], :]  # W,Q(L)
            N = A.shape[0]
            sKs: [SparseTensor] = self.kernels[0].forward_sparse(N, valD, idx)

            Ks = torch.stack([sk.to_dense() for sk in sKs], dim=0)  # M,N,N
            return Ks

    def forward(self, X, idx, valD) -> torch.Tensor:
        """
        :param X: N,df
        :param A: N,N
        :param D: Q,N,N
        :return:
        """
        y = X
        for i in range(self.T):
            kernel: StrapKernel = self.kernels[i]
            psi = self.psis[i]

            sK = kernel.forward_sparse(idx, valD)
            y = torch.sparse.mm(sK, y)
            y = psi(y)

        y = self.top(y)
        return y

    def fix_kernel(self, flag):
        for kernel in self.kernels:
            kernel.fix_parameters(flag)

    def load_kernel(self, path):
        # the parameters needs to be the same
        self.kernel.load_state_dict(torch.load(path))

    def expoert_kernel(self) -> [dict]:
        dicts = []
        for kernel in self.kernels:
            dicts.append(deepcopy(kernel.state_dict()))
        return dicts

    def import_kernel(self, dicts):
        for i in range(self.T):
            state = dicts[i]
            kernel = self.kernels[i]
            kernel.load_state_dict(state)
