# check whether gnc can predict motif counts for each node

import networkx as nx
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F


from core.SparseTensor import is_sparse, SparseTensor
from data.Data  import Data
from core.graph_util import generate_sparse_kernel
from core.StrapKernel import StrapKernel

from model.SuperNCModel import gen_psi


class FixedNCModel(nn.Module):

    def __init__(self, K, df, **parameters):
        super().__init__()
        self.K = K # output dim
        self.df = df

        # save to self
        for parameter, value in parameters.items():
            setattr(self, parameter, value)

        assert self.psi_p > 0, (self.psi_p)

        psis = []
        for i in range(self.T):
            d_in = df if i == 0 else self.psi_d
            d_out = K if i == self.T - 1 else self.psi_d
            nonlinear = False if i == self.T - 1 else True
            psi = gen_psi(self.psi_p, d_in, self.psi_d, d_out, self.psi_do, nonlinear)
            psis.append(psi)
        self.ml_psis = nn.ModuleList(psis)
        self.psis = psis

        # classifier
        self.top = nn.LogSoftmax(dim=1)

        self.fixed_strap = None

        self.kernel:SparseTensor = None

    def generate_sparse_kernel(self, A:SparseTensor) -> SparseTensor:

        kernel_name = self.kernel_name #'tA'
        K = generate_sparse_kernel(A, kernel_name)

        # power
        if self.S > 1:
            Kp = K.to_dense()
            for i in range(2, self.S + 1):
                Kp = torch.sparse.mm(K, Kp)
            K = Kp.to_sparse()

        return K

    def generate_kernel(self, A: torch.Tensor) -> torch.Tensor:
        return self.generate_sparse_kernel(A.to_sparse()).to_dense().unsqueeze(0)


    def convert_train(self, data: Data) -> (torch.Tensor, SparseTensor):
        X = data.features
        self.kernel = self.generate_sparse_kernel(data.A)
        X = torch.sparse.mm(self.kernel, X)
        return X, self.kernel


    def forward(self, X: torch.Tensor, kernel: SparseTensor) -> torch.Tensor:
        """
        :param X: N,df
        :return:
        """
        #self.kernel = kernel
        y = X
        y = self.psis[0](y)
        for psi in self.psis[1:]:
            y = torch.sparse.mm(kernel, y)
            y = psi(y)
        y = self.top(y)
        return y
