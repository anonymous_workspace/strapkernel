# check whether gnc can predict motif counts for each node


import networkx as nx
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F


from core.ChebKernel import ChebKernel
from core.SparseTensor import is_sparse, SparseTensor
from data.Data  import Data
from model.SuperNCModel import gen_psi

class ChebNCModel(nn.Module):

    def __init__(self, K, df, **parameters):
        super().__init__()
        #df = df+1
        self.K = K # output dim = number of classes
        self.df = df # input dim of X

        # save to self
        for parameter, value in parameters.items():
            setattr(self, parameter, value)

        kernels = []
        psis = []
        for i in range(self.T):
            d_in = df if i == 0 else self.psi_d
            d_out = K if i == self.T - 1 else self.psi_d
            nonlinear = False if i == self.T - 1 else True


            kernel  = ChebKernel(self.S)
            psi = gen_psi(self.psi_p, d_in, self.psi_d, d_out, self.psi_do, nonlinear)

            kernels.append(kernel)
            psis.append(psi)
            #self.register_parameter('conv' + str(i), conv)

        # regeister as parameters
        self.ml_kernels = nn.ModuleList(kernels)
        self.kernels = kernels
        self.ml_psis = nn.ModuleList(psis)
        self.psis = psis

        # classifier
        self.top = nn.LogSoftmax(dim=1)


    def generate_kernel(self, A: torch.Tensor) -> torch.Tensor:
        with torch.no_grad():
            self.eval()
            sA = A.to_sparse()
            D = ChebKernel.base_Ls(sA, self.S)
            M = ChebKernel.mask_from_A(A, self.S)

            C: SparseTensor = M.to_sparse()
            idx = C._indices()  # 2, W
            valD = D[idx[0], idx[1], :]  # W,Q(L)
            N = A.shape[0]
            sKs: [SparseTensor] = self.kernels[0].forward_sparse(N, valD, idx)
            Ks = torch.stack([sk.to_dense() for sk in sKs], dim=0) # M,N,N
            return Ks

    def convert_train(self, data: Data) -> (torch.Tensor, torch.Tensor, torch.Tensor):
        # pre-compute inputs
        X = data.features
        sA = data.A
        A = sA.to_dense()
        D = ChebKernel.base_Ls(sA, self.S) # torch.Tensor
        M = ChebKernel.mask_from_A(A, self.S)

        C: SparseTensor = M.to_sparse()
        idx = C._indices() # 2, W
        valD = D[idx[0],idx[1],:] # W,Q(L)
        return X, idx, valD

    def forward(self, X, idx, valD) -> torch.Tensor:
        y = X
        for i in range(self.T):
            kernel:ChebKernel = self.kernels[i]
            psi = self.psis[i]
            sK = kernel.forward_sparse(idx, valD) # SparseTensor
            y = torch.sparse.mm(sK, y) # N, df

            y = psi(y)
        y = self.top(y)
        return y
