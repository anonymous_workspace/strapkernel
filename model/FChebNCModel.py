# check whether gnc can predict motif counts for each node


import networkx as nx
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F


from core.ChebKernel import ChebKernel
from data.Data  import Data

from model.SuperNCModel import gen_psi


class FChebNCModel(nn.Module):

    def __init__(self, K, df, **parameters):
        super().__init__()

        self.K = K # output dim = number of classes
        self.df = df # input dim of X

        # save to self
        for parameter, value in parameters.items():
            setattr(self, parameter, value)

        assert self.T == 1, 'only for T = 1'

        self.w = nn.Parameter(torch.rand((self.S + 1), dtype=torch.float))
        self.psi = gen_psi(self.psi_p, df, self.psi_d, K, self.psi_do, nonlinear=False)

        # kernels = []
        # psis = []
        # for i in range(self.T):
        #     d_in = df if i == 0 else self.psi_d
        #     d_out = K if i == self.T - 1 else self.psi_d
        #     nonlinear = False if i == self.T - 1 else True
        #
        #
        #     kernel  = ChebKernel(self.M, self.S)
        #     psi = gen_psi(self.psi_p, d_in, self.psi_d, d_out, self.psi_do, nonlinear)
        #
        #     kernels.append(kernel)
        #     psis.append(psi)
            #self.register_parameter('conv' + str(i), conv)

        # # regeister as parameters
        # self.ml_kernels = nn.ModuleList(kernels)
        # self.kernels = kernels
        # self.ml_psis = nn.ModuleList(psis)
        # self.psis = psis

        # classifier
        self.top = nn.LogSoftmax(dim=1)


    def generate_kernel(self, A: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError

    def convert_train(self, data: Data) -> [torch.Tensor]:
        # pre-compute inputs
        X = data.features # N,d
        sA = data.raw_adj
        D = ChebKernel.base_Ls(sA, self.S) # torch.Tensor N,N,(S+1)

        DX = [torch.matmul(D[:,:,i], X) for i in range(self.S + 1)]
        DX = torch.stack(DX, dim=0) # S,N,d
        return [DX]

    def forward(self, DX) -> torch.Tensor:
        """
        :param X: N,df
        :param A: N,N
        :param sDs: Q,N,N
        :return:
        """
        w = self.w.unsqueeze(1).unsqueeze(1) # S+1,1,1
        y = torch.sum(DX * w, dim=0) # N,df
        y = self.psi(y)
        y = self.top(y)
        return y
