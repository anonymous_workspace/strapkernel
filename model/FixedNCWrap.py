from model.SuperNCWrap import SuperNCWrap
from model.FixedNCModel import FixedNCModel


class FixedNCWrap(SuperNCWrap):

    def __init__(self, K: int, df: int, **parameters):
        self.K = K
        self.df = df
        self.S = 1
        self.T = 1
        self.psi_p = 1
        self.psi_d = 16
        self.psi_do = 0.2
        super().__init__()
        self.set_params(**parameters)

    def get_params(self, deep=True):
        prms = super().get_params(deep)
        prms["K"] = self.K
        prms["df"] = self.df
        prms["S"] = self.S
        prms["T"] = self.T
        prms["psi_p"] = self.psi_p
        prms["psi_d"] = self.psi_d
        prms["psi_do"] = self.psi_do
        return prms

    def set_params(self, **parameters):
        super().set_params(**parameters)
        self.model = FixedNCModel(self.K, self.df, S=self.S, T=self.T, kernel_name=self.kernel_name, psi_p=self.psi_p,
                                  psi_d=self.psi_d, psi_do=self.psi_do)
        return self
